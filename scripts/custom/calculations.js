function generateResultAsTable(uploadedData, allRates) {
    var daysWithAmounts = calculate(uploadedData, allRates);
    var maxTotalAmountDays = findMaxTotalAmountDays(daysWithAmounts);
    generateTable(maxTotalAmountDays);
}

function calculate(uploadedData, allRates) {
// 1. Prvo ide izračun svega


    var COMMA_WHITESPACE = ',';
    var daysWithTotalAmounts = [];
    for (var i = 0; i < allRates.length; i++) {
        var totalAmount = 0;
        var usedCurrencies = '';
        for (var j = 0; j < uploadedData.length; j++) {
            if (uploadedData[j].currency !== allRates[i].base) {
                totalAmount += uploadedData[j].amount / allRates[i].rates[uploadedData[j].currency];
                usedCurrencies += uploadedData[j].currency + COMMA_WHITESPACE;
            }
        }
        daysWithTotalAmounts.push({
            Date: allRates[i].date,
            TotalAmount: totalAmount,
            CurrenciesList: getCorrectColumnContent(usedCurrencies, COMMA_WHITESPACE)
        });
    }

    return daysWithTotalAmounts;

}


function findMaxTotalAmountDays(daysWithAmountsData) {
    // 2. Bira se 5 najvećih total amounta

    function sortByTotalAmount(a, b) {
        return b.TotalAmount - a.TotalAmount
    }

    var NUMBER_OF_DAYS = 5;

    var sortedData = daysWithAmountsData.sort(sortByTotalAmount);

    var splicedData = sortedData.splice(0, NUMBER_OF_DAYS);

    return splicedData;


}


function generateTable(data) {
// 3. Podaci idu u tablice

    if ($('table').length > 0) {
        $('table').remove();
    }


    var columnNames = Object.keys(data[0]);

    var table = document.createElement('table');

    var thead = document.createElement('thead');
    table.appendChild(thead);
    for (var i = 0; i < columnNames.length; i++) {
        var th = document.createElement("th");
        var thCell = document.createTextNode(columnNames[i]);
        th.appendChild(thCell);
        thead.appendChild(th);
    }


    var tableBody = document.createElement('tbody');

    for (var i = 0; i < data.length; i++) {
        var tr = document.createElement('tr');

        for (var j = 0; j < columnNames.length; j++) {
            var td = document.createElement('td');
            var cell = document.createTextNode(data[i][columnNames[j]]);
            td.appendChild(cell);
            tr.appendChild(td);
        }

        tableBody.appendChild(tr);
    }

    table.appendChild(tableBody);
    document.body.appendChild(table);

    table.setAttribute('border', '1');

}


