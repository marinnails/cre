function simpleFileUploader(noHashID, onlyOneFormatString, onLoadEndFunction) {
    var fileUploader = document.getElementById(noHashID);

    if (fileUploader.length > 0) {
        return;
    }

    function handleFileSelect() {
        var files = fileUploader.files; // FileList object
        var file = files[0];

        if (!files.length) {
            console.error('Please select a file!');
            return;
        }
        // Only process JSON file.
        if (!file.name.match(onlyOneFormatString)) {
            console.error('Please select a' + onlyOneFormatString + 'file!');
            return;
        }

        var reader = new FileReader();

        reader.onloadend = function (event) {
            if (event.target.readyState == FileReader.DONE) {
                onLoadEndFunction(event);
            }
        };
        reader.readAsBinaryString(file);
    }

    fileUploader.addEventListener('change', handleFileSelect, false);
}


//Ideja: ovo kasnije pretvoriti u klase
function simpleDropDownList(noHashID, placeholder, data, changeFunction) {
    var dropDownList = document.getElementById(noHashID);

    if (dropDownList.length > 0) {
        return;
    }

    var optionLabel = document.createElement('option');
    optionLabel.value = placeholder;
    optionLabel.innerHTML = placeholder;
    dropDownList.appendChild(optionLabel);

    for (var i = 0; i < data.length; i++) {
        var opt = document.createElement('option');
        opt.value = data[i];
        opt.innerHTML = data[i];
        dropDownList.appendChild(opt);
    }


    dropDownList.addEventListener('change', changeFunction, false);

}