// region constants
var currencies = ['EUR', 'USD', 'JPY', 'CAD', 'CHF', 'GBP'];
//endregion

// region currency data
//Ideja: ovo bi mogli u local storage, i ako nije današnji dan, pozivati api, a ako je današnji dan, vraćati što je u local storageu
function getRates(currentDate, baseCurrency) {
    var API_LAYER = 'http://api.fixer.io';
    var CURRENCIES_LIST = currencies.toString();

    try{
        return $.ajax({
            dataType: 'jsonp',
            url: API_LAYER + '/' + currentDate + '?base=' + baseCurrency + '&symbols=' + CURRENCIES_LIST
        });
    }catch(e){
           console.error("Too much queries! Try again!");
    }

}

function getAllCurrencyRates(baseCurrency, rangeOfDays) {

    var formattedDates = getFormattedDatesByRangeOfDays(rangeOfDays);

    var allCurrentRates = [];
    for (var i = 0; i < formattedDates.length; i++) {
        getRates(formattedDates[i], baseCurrency).done(function (currentData) {
            allCurrentRates.push(currentData);
        });
    }

    return allCurrentRates;

}


function loadAllCurrencyRates(selectedCurrencyOption){
    try {

        var RANGE_OF_DAYS = 30;
        var TOTAL_COUNT = RANGE_OF_DAYS + 1;
        var allCurrencyRates = getAllCurrencyRates(selectedCurrencyOption, RANGE_OF_DAYS);

        return loadData(allCurrencyRates, TOTAL_COUNT);

    }catch(e){
        console.error("Not all queries are executed.");
    }
}

//endregion

// region local storage

var LOCAL_STORAGE_DATA_KEY = 'uploaded-data';

function getAllUploadedData() {

    try {

        var data = window.localStorage.getItem(LOCAL_STORAGE_DATA_KEY);
        var parsedData = JSON.parse(data);
        return parsedData;

    } catch (e) {

        console.error("Malformed data in local storage. Please try again.");
        window.localStorage.removeItem(LOCAL_STORAGE_DATA_KEY);

    }

}

function setUploadedDataIntoLocalStorage(currentlyUploadedFileData) {

    try{
        JSON.parse(currentlyUploadedFileData);
    }catch (e){
        window.localStorage.removeItem(LOCAL_STORAGE_DATA_KEY);
        location.reload();
    }

    try {

        var uploadedData = window.localStorage.getItem(LOCAL_STORAGE_DATA_KEY);

        if (uploadedData === null) {
            window.localStorage.setItem(LOCAL_STORAGE_DATA_KEY, currentlyUploadedFileData);
        } else {
            var parsedUploadedData = JSON.parse(uploadedData);
            var currentlyParsedData = JSON.parse(currentlyUploadedFileData);
            var result = parsedUploadedData.concat(currentlyParsedData);
            window.localStorage.setItem(LOCAL_STORAGE_DATA_KEY, JSON.stringify(result));
        }

    } catch (e) {
        console.error("Malformed data in local storage. Please try again.");
        window.localStorage.removeItem(LOCAL_STORAGE_DATA_KEY);
    }

}

// endregion

