function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}


function getFormattedDatesByRangeOfDays(rangeOfDays) {
    var formattedDates = [];
    for (var i = 0; i <= rangeOfDays; i++) {
        var startDate = new Date();
        var date = startDate.setDate(startDate.getDate() - i);
        var formatedDate = formatDate(date);
        formattedDates.push(formatedDate);
    }

    return formattedDates;
}


// Nužno ako želimo dohvatiti podatke od 30-ak upita, a da smo sigurni da su svi dohvaćeni.
// Alternativa je da se piše svaki upit zasebno.
function loadData(data, conditionTotalCount) {
    var INTERVAL_UNIT = 500;
    var defer = $.Deferred();
    var intervalID = window.setInterval(function () {
        if (data.length === conditionTotalCount) {
            clearInterval(intervalID);
            defer.resolve(data)
        }
    }, INTERVAL_UNIT);

    return defer.promise();
}

function getCorrectColumnContent(data, delimiter) {
    var slicedData = data.slice(0, data.length - delimiter.length);
    var splitedData = slicedData.split(delimiter);
    var uniquedData = $.unique(splitedData);
    var stringData = uniquedData.toString();
    return stringData;
}



