// Check for the various File API support.
if (window.File && window.FileReader && window.FileList && window.Blob) {

    simpleFileUploader('fileUploader', '.json', function (e) {
        var currentlyUploadedFileData = e.target.result;

        setUploadedDataIntoLocalStorage(currentlyUploadedFileData);

        $('#currencySelect').show();

        var PLACEHOLDER_TEXT = 'Choose';
        simpleDropDownList('currencySelect', PLACEHOLDER_TEXT, currencies, function (e) {
            var selectedCurrencyOption = e.target.value;
            if (selectedCurrencyOption === PLACEHOLDER_TEXT) {
                return;
            }
            $('#currencySelect').attr('disabled', true);

            loadAllCurrencyRates(selectedCurrencyOption).done(function (allCurrencyRatesData) {

                var parsedFileData = getAllUploadedData();
                $('#currencySelect').attr('disabled', false);
                generateResultAsTable(parsedFileData, allCurrencyRatesData);
            });

        });

    })


} else {
    console.error('The File APIs are not fully supported in this browser.');
}









